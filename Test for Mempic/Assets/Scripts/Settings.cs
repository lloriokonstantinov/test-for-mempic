﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Settings : ScriptableObject
{
    #region Singletone
    private static Settings instance;
    public static Settings Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<Settings>("Settings");
            }
            return instance;
        }
    }
    #endregion

    public float CylinderHeight = .25f;
    public float CylinderWidthMin = .1f;
    public float CylinderWidthMaxAllowed = 1f;
    public float CylinderWidthMaxForLoose = 1.1f;
    public float ScaleSpeedWhenTap = 2f;
    public float PerfectMoveBorder = .05f;

    public float PerfectMoveScaleSpeed = 2f;
    public float PerfectMoveScaleBonusForFirst = .2f;
    public float PerfectMoveScaleForOther = .8f;
    public float PerfectMoveScaleAnimatedForFirst = .4f;
    public float PerfectMoveScaleAnimatedForOther = .3f;

    public float CameraDistance = -7f;
    public float CameraYOffset = 2f;

    public float WaveStep = .2f;

}
