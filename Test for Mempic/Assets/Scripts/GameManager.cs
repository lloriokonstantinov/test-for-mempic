﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    private bool inputEnabled = true;

    private Action OnMouseDown;
    private Action OnMouseUp;

    private bool wasMouseDown = false;

    private void Awake()
    {
        ResourcesManager.InitResourses();
    }

    private void Start()
    {
        SetGameControls();
        Tower.Instance.OnAnimationStart += () => EnableControls(false); 
        Tower.Instance.OnAnimationEnd += () => EnableControls(true);
        Tower.Instance.OnCylinderFailed += () => StartCoroutine(GameOver());
    }

    private void Update()
    {
        if (!inputEnabled)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            wasMouseDown = true;
            OnMouseDown.Invoke();
        }
        if (Input.GetMouseButtonUp(0) && wasMouseDown)
        {
            OnMouseUp.Invoke();
        }
    }

    private void EnableControls(bool enabled)
    {
        inputEnabled = enabled;
        wasMouseDown = false;
    }

    private IEnumerator GameOver()
    {
        EnableControls(false);
        yield return MainCamera.Instance.MoveForPanoram();
        SetStartControls();
        EnableControls(true);
    }

    private void SetGameControls()
    {
        wasMouseDown = false;
        OnMouseDown = Tower.Instance.AddActiveCylinder;
        OnMouseUp = Tower.Instance.FixActiveCylinder;
    }

    private void SetStartControls()
    {
        wasMouseDown = false;
        OnMouseDown = () =>
        {
            Tower.Instance.Refresh();
            SetGameControls();
        };
        OnMouseUp = () => { };
    }

}
