﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResourcesManager 
{
    public static Material SimpleMaterial;
    public static Material RedMaterial;

    public static void InitResourses()
    {
        Pool.Instance.InitObject(Resources.Load<Cylinder>("Prefabs/Cylinder"), 20);
        SimpleMaterial = Resources.Load<Material>("Materials/SimpleMaterial");
        RedMaterial = Resources.Load<Material>("Materials/RedMaterial");
    }
}
