﻿public interface IScalable 
{
    float Size { get; set; }
}
