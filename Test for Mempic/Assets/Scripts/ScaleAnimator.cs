﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAnimator : MonoBehaviour
{
    private IScalable target;

    private Queue<AnimData> animDatas;

    private bool isPlaying;
    public void Initialize(IScalable scalable)
    {
        target = scalable;
        animDatas = new Queue<AnimData>();
        isPlaying = false;
    }

    public void AddAnimationToQueve(float startSize, float targetSize, float speed, Action endCallback = null)
    {
        animDatas.Enqueue(new AnimData(startSize, targetSize, speed, endCallback));
        if (!isPlaying)
        {
            isPlaying = true;
            StartCoroutine(MainCoroutine());
        }
    }

    public void StopAnimation()
    {
        StopAllCoroutines();
        animDatas.Clear();
        isPlaying = false;
    }

    private IEnumerator MainCoroutine()
    {
        while (animDatas.Count > 0)
        {
            yield return StartCoroutine(ScaleAnimation(animDatas.Dequeue()));
        }
        isPlaying = false;
    }

    private IEnumerator ScaleAnimation(AnimData parameters)
    {
        var waiter = new WaitForEndOfFrame();
        float animStep = parameters.StartSize;
        float sign = parameters.StartSize - parameters.TargetSize > 0 ? -1 : 1;
        while (animStep * sign < parameters.TargetSize * sign)
        {
            animStep += Time.deltaTime * parameters.Speed * sign;
            target.Size = animStep;
            yield return waiter;
        }
        target.Size = parameters.TargetSize;

        if (parameters.EndCallback != null)
        {
            parameters.EndCallback.Invoke();
        }

    }

    public struct AnimData
    {
        public float StartSize;
        public float TargetSize;
        public float Speed;
        public Action EndCallback;

        public AnimData(float startSize, float targetSize, float speed, Action endCallback)
        {
            StartSize   = startSize;
            TargetSize  = targetSize;
            Speed       = speed;
            EndCallback = endCallback;
        }

    }
}

