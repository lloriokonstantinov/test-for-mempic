﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    #region Singletone
    private static Tower instance;
    public static Tower Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Tower>();
            }
            return instance;
        }
    }
    #endregion

    private List<Cylinder> towerCylinders;

    private Cylinder activeCylinder;

    private int lastPerfectIndex = 0;

    public float CurrentHeight
    {
        get
        {
            return towerCylinders.Count * Settings.Instance.CylinderHeight * 2;
        }
    }

    public event Action<Cylinder> OnFixCylinder = (c) => { };
    public event Action OnAnimationStart = () => { };
    public event Action OnAnimationEnd = () => { };
    public event Action OnCylinderFailed = () => { };

    private void Awake()
    {
        towerCylinders = new List<Cylinder>();
    }

    private void Start()
    {
        SetFirstCylinder();
    }

    public void Refresh()
    {
        foreach (var cyl in towerCylinders)
        {
            Pool.Instance.Put(cyl);
        }
        towerCylinders.Clear();
        lastPerfectIndex = 0;
        SetFirstCylinder();
    }

    public void SetFirstCylinder()
    {
        var cylinder = CreateCylinder(false);
        OnFixCylinder(cylinder);
    }

    public void AddActiveCylinder()
    {
        activeCylinder = CreateCylinder();
        activeCylinder.OnPerfectMove += StartPerfectMoveWave;
        activeCylinder.OnScaleOverRate += RunFailAnimation;
    }

    private Cylinder CreateCylinder(bool isScaling = true)
    {
        var cylinder = Pool.Instance.Take("cylinder") as Cylinder;
        float previosSize = towerCylinders.Count > 0 ? towerCylinders[towerCylinders.Count - 1].Size : 0f;
        cylinder.Initialize(CurrentHeight, previosSize, isScaling);
        cylinder.transform.SetParent(transform);
        towerCylinders.Add(cylinder);       
        return cylinder;
    }


    public void FixActiveCylinder()
    {
        activeCylinder.StopScaling();
        OnFixCylinder(activeCylinder);
    }

    public void StartPerfectMoveWave()
    {
        StartCoroutine(ScaleWave());
    }
    public void RunFailAnimation()
    {
        StartCoroutine(FailAnimation());
    }

    public IEnumerator ScaleWave()
    {
        OnAnimationStart();

        var settings = Settings.Instance;

        var intermediateSize = activeCylinder.Size + settings.PerfectMoveScaleAnimatedForFirst;
        activeCylinder.AddAnimation(activeCylinder.Size, intermediateSize, settings.PerfectMoveScaleSpeed);

        var finalSize = activeCylinder.Size + settings.PerfectMoveScaleBonusForFirst;
        finalSize = Mathf.Clamp(finalSize, settings.CylinderWidthMin, settings.CylinderWidthMaxAllowed);
        activeCylinder.AddAnimation(intermediateSize, finalSize, settings.PerfectMoveScaleSpeed, () => { OnAnimationEnd(); });

        var waiter = new WaitForSeconds(settings.WaveStep);

        for (int i = towerCylinders.Count - 2; i >= 0; i--)
        {
            intermediateSize = towerCylinders[i].Size + settings.PerfectMoveScaleAnimatedForOther;
            towerCylinders[i].AddAnimation(towerCylinders[i].Size, intermediateSize, settings.PerfectMoveScaleSpeed);

            finalSize = i > lastPerfectIndex ? (towerCylinders[i].Size * settings.PerfectMoveScaleForOther) : (towerCylinders[i].Size);
            finalSize = Mathf.Clamp(finalSize, settings.CylinderWidthMin, settings.CylinderWidthMaxAllowed);
            towerCylinders[i].AddAnimation(intermediateSize, finalSize, settings.PerfectMoveScaleSpeed);

            yield return waiter;
        }

        lastPerfectIndex = towerCylinders.Count;
        
    }

    public IEnumerator FailAnimation()
    {
        OnAnimationStart();
        activeCylinder.MarkRed();
        yield return new WaitForSeconds(.4f);
        Pool.Instance.Put(activeCylinder);
        towerCylinders.Remove(activeCylinder);
        OnAnimationEnd();
        OnCylinderFailed();
    }

}
