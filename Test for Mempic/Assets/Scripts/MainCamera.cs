﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{

    #region Singletone
    private static MainCamera instance;
    public static MainCamera Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<MainCamera>();
            }
            return instance;
        }
    }
    #endregion

    private float cameraSpeed = 3f;

    private void Start()
    {
        Tower.Instance.OnFixCylinder += SetNewCylinderTarget;
    }

    private void SetNewCylinderTarget(Cylinder cylinder)
    {
        var endPos = new Vector3(0, cylinder.transform.position.y + Settings.Instance.CameraYOffset, Settings.Instance.CameraDistance);
        StartCoroutine(MoveToTarget(endPos));
    }


    private IEnumerator MoveToTarget(Vector3 position)
    {
        float animStep = 0f;
        var waiter = new WaitForEndOfFrame();
        var startPos = transform.position;
        while (animStep < 1f)
        {
            transform.position = Vector3.Lerp(startPos, position, animStep);
            animStep += Time.deltaTime * cameraSpeed;
            yield return waiter;
        }
        transform.position = position;
    }

    public Coroutine MoveForPanoram()
    {
        var angle = Camera.main.fieldOfView;
        var towerHeight = Tower.Instance.CurrentHeight;
        var z = towerHeight * Mathf.Atan(angle * Mathf.Deg2Rad) + 2f;
        var position = new Vector3(0, Tower.Instance.CurrentHeight/2 , -z);

        return StartCoroutine(MoveToTarget(position));
    }

}
