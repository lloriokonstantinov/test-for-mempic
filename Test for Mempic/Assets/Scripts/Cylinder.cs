﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cylinder : MonoBehaviour, IPoolable, IScalable
{
    [SerializeField] private ScaleAnimator scaleAnimator = null;
    [SerializeField] private new Renderer renderer = null;

    #region IPoolable
    [SerializeField] private string poolableName = "";
    string IPoolable.Name => poolableName;

    void IPoolable.Activate()
    {

        gameObject.SetActive(true);
        transform.SetParent(null);
    }

    void IPoolable.ReturnToPool(Transform poolParent)
    {
        gameObject.SetActive(false);
        transform.SetParent(poolParent);
        OnScaleOverRate = () => { };
        OnPerfectMove = () => { };
    }
    #endregion

    public event Action OnScaleOverRate = () => { };
    public event Action OnPerfectMove = () => { };


    private float size;
    public float Size
    {
        get
        {
            return size;
        }
        set
        {
            float width = value > Settings.Instance.CylinderWidthMin ? value : Settings.Instance.CylinderWidthMin;                                             
            size = width;
            transform.localScale = new Vector3(width, Settings.Instance.CylinderHeight, width);
        }
    }

    private float previosSize;

    private void Awake()
    {
        if (scaleAnimator == null)
        {
            scaleAnimator = GetComponent<ScaleAnimator>() ?? gameObject.AddComponent<ScaleAnimator>();
        }
        if (renderer == null)
        {
            renderer = GetComponent<Renderer>();
        }

        scaleAnimator.Initialize(this);
    }

    public void Initialize(float Ypos, float previosSize, bool isScaling = true)
    {
        this.previosSize = previosSize;
        transform.position = Vector3.up * Ypos;

        renderer.material = ResourcesManager.SimpleMaterial;

        if (isScaling)
        {
            StartScaling();
        }
        else
        {
            Size = Settings.Instance.CylinderWidthMaxAllowed;
        }

    }

    public void StartScaling()
    {
        var settings = Settings.Instance;
        AddAnimation(settings.CylinderWidthMin,
                     settings.CylinderWidthMaxForLoose * previosSize,
                     settings.ScaleSpeedWhenTap,
                     () => { OnScaleOverRate(); });
    }

    public void AddAnimation(float startSize, float targetSize, float speed, Action endCallback = null)
    {
        scaleAnimator.AddAnimationToQueve(startSize, targetSize, speed, endCallback);
    }

    public void StopScaling()
    {
        scaleAnimator.StopAnimation();
        if (Size > previosSize)
        {
            OnScaleOverRate();
        }
        else if (Size > previosSize - Settings.Instance.PerfectMoveBorder)
        {
            OnPerfectMove();
        }
    }

    public void MarkRed()
    {
        renderer.material = ResourcesManager.RedMaterial;
    }

}
